# This file contains all the necessary credentials
# Copy this file and rename it 'credentials.py'
# Command : `cp credentials.example.py  credentials.py`

# SALESFORCE_HOSTNAME = ''
# SALESFORCE_EMAIL = ''
# SALESFORCE_PASSWORD = ''
# SALESFORCE_SECURITY_TOKEN = ''
# SALESFORCE_API_VERSION = '' # Version should be a string containing a "." for example '46.0'
# SALESFORCE_CLIENT_ID = 'OpsEtl' # Do not rename

# MYSQL_HOST = ''
# MYSQL_USER = ''
# MYSQL_PASSWORD = ''
# MYSQL_DB = ''
# MYSQL_CHARSET = ''

# REDASH_API_KEY = ''
# REDASH_HOST = ''

# SLACK_TOKEN = ''
# SLACK_CHANNEL = ''
# SLACK_ICON = ''
# SLACK_USERNAME = ''