import sys
import time
import logging

sys.path.append(".") # Adds higher directory to python modules path.

from modules.salesforce import Salesforce
from modules.redash import Redash
from modules.slack import Slack


def redash_to_salesforce_update(query_id, object_api_name):
    # Slack().send_slack(":spinner: RUNNING REDASH TO SALESFORCE UPDATE FROM QUERY " + str(query_id))
    redash = Redash()
    data = redash.get_query_result(query_id)
    salesforce = Salesforce()
    salesforce.update_records(object_api_name, data)
    # Slack().send_slack(":heavy_check_mark: DELETE DONE 0 ERRORS")


def redash_to_salesforce_upsert(query_id, object_api_name):
    # Slack().send_slack(":spinner: RUNNING REDASH TO SALESFORCE UPDATE FROM QUERY " + str(query_id))
    redash = Redash()
    data = redash.get_query_result(query_id)
    salesforce = Salesforce()
    salesforce.upsert_records(object_api_name, data, 'Id')
    # Slack().send_slack(":heavy_check_mark: DELETE DONE 0 ERRORS")


def redash_to_salesforce_delete(query_id, object_api_name):
    # Slack().send_slack(":spinner: RUNNING REDASH TO SALESFORCE UPDATE FROM QUERY " + str(query_id))
    redash = Redash()
    data = redash.get_query_result(query_id)
    salesforce = Salesforce()
    salesforce.delete_records(object_api_name, data, True)
    # Slack().send_slack(":heavy_check_mark: DELETE DONE 0 ERRORS")

if __name__ == '__main__':
    # 499 is a Re:Dash Query ID
    redash_to_salesforce_update(499, 'Account')