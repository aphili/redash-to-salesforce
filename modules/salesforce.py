from simple_salesforce import Salesforce as SimpleSalesforce
from credentials import SALESFORCE_HOSTNAME, SALESFORCE_EMAIL, SALESFORCE_PASSWORD, SALESFORCE_SECURITY_TOKEN, SALESFORCE_API_VERSION, SALESFORCE_CLIENT_ID
from collections import OrderedDict
import pandas as pd
import json


class Salesforce:

    # Init function
    def __init__(self):
        self.instance = SALESFORCE_HOSTNAME
        self.username = SALESFORCE_EMAIL
        self.password = SALESFORCE_PASSWORD
        self.security_token = SALESFORCE_SECURITY_TOKEN
        self.api_version = SALESFORCE_API_VERSION
        self.client_id = SALESFORCE_CLIENT_ID

    def get_sf(self):
        sf = SimpleSalesforce(
            instance=self.instance,
            username=self.username,
            password=self.password,
            security_token=self.security_token,
            version=self.api_version,
            client_id=self.client_id)
        return sf

    def clean_values(self, values):
        values = values['records']
        for value in values:
            value.pop("attributes", None)  # Removes OrderedDict attributes
        values = json.loads(json.dumps(values))  # Converts OrderedDict to JSON
        return values

    def get_records(self, query):
        sf = self.get_sf()
        response = sf.query_all(query)
        result = self.clean_values(response)
        return result

    def update_records(self, object_name, data):
        sf = self.get_sf()
        sf.bulk.__getattr__(object_name).update(data)

    def upsert_records(self, object_name, data, external_id):
        sf = self.get_sf()
        sf.bulk.__getattr__(object_name).upsert(data, external_id)

    def delete_records(self, object_name, data, soft_delete=True):
        sf = self.get_sf()
        if(soft_delete):
            sf.bulk.__getattr__(object_name).delete(data)
        else:
            sf.bulk.__getattr__(object_name).hard_delete(data)
        
    def result_to_df(self, result):
        df = pd.json_normalize(result)
        return df
