from redashAPI.client import RedashAPIClient
from credentials import REDASH_API_KEY, REDASH_HOST
import requests
import pandas as pd


class Redash:

    def __init__(self):
        self.api_key = REDASH_API_KEY
        self.host = REDASH_HOST

    def get_redash(self):
        return RedashAPIClient(self.api_key, self.host)

    def get_query_key(self, query_id):
        redash = self.get_redash()
        result = redash.get('queries/'+str(query_id))
        return result.json()['api_key']

    def get_query_result(self, query_id):
        api_key = self.get_query_key(query_id)
        result = requests.get(self.host + '/api/queries/' + str(query_id) + '/results.json?api_key=' + api_key).json()
        return result['query_result']['data']['rows']
        
    def result_to_df(self, result):
        df = pd.json_normalize(result)
        return df