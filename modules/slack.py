from slacker import Slacker
from credentials import SLACK_TOKEN, SLACK_CHANNEL, SLACK_ICON, SLACK_USERNAME

class Slack:

    def __init__(self):
        self.token = SLACK_TOKEN
        self.channel = SLACK_CHANNEL
        self.icon = SLACK_ICON
        self.username = SLACK_USERNAME

    def get_slack(self):
        return Slacker(self.token)

    def send_slack(self, message):
        slack = self.get_slack()
        slack.chat.post_message(channel=self.channel, text=message, username=self.username, icon_url=self.icon)

    