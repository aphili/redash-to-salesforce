# **Re:Dash <> Salesforce ETL (and a bit more...)**
 

> ***Disclaimer** : This project is a **work in progress**. Debugging your queries is a pain since no error messages are currently logged as of the 03/12/2020.*

--- 


## 1. Install 

In order to run this script, you must be using Python3.8 or above.

```sh 
git clone
cd redash_to_salesforce
cp credentials.example.py credentials.py
# fill in your credentials

pip3 install -r requirements.txt
python3 scripts/redash_to_salesforce.py
```

## 2. Examples

#### From Salesforce SOQL query to JSON result

```python
from salesforce import Salesforce

salesforce = Salesforce()
query = "SELECT Id, Name, OwnerId, BillingStreet, BillingCity, BillingCountry FROM Account LIMIT 10"
result = salesforce.get_records(query)

```

#### From ReDash query to JSON result

```python
from redash import Redash

zenchef = Redash()
result = redash.get_query_result(query_id)
```

#### Update, Upsert or Delete records in Salesforce

```python
from salesforce import Salesforce

salesforce = Salesforce()
data = [
    {"id":"record_id", "name":"record_name"}
]

# UPDATE
salesforce.update_records(object_name, data)
# UPSERT
salesforce.upsert_records(object_name, data, external_id)
# DELETE
salesforce.delete_records(object_name, data)
```

#### Update, Upsert or Delete records from Re:Dash in Salesforce

> ***Information*** : *For this script to work, you'll need to prepare a query where the column names **correspond exactly** to the field API names of the ones you're trying to update*.  

```python
from salesforce import Salesforce
from redash import Redash

redash = Redash()
data = redash.get_query_result(query_id)
salesforce = Salesforce()
salesforce.update_records(object_api_name, data)
```

#### How to run it on a fresh AWS EC2 instance ?

###### Install python3.7 & pip3.7

```bash
sudo apt-get update
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.7
sudo apt-get update
python3.7 get-pip.py
```

###### Uninstall / install libraries 

This step is necessary otherwise the script will not run.

```bash
pip3.7 uninstall pyopenssl
pip3.7 uninstall cryptography
pip3.7 install pyopenssl
pip3.7 install cryptography
pip3.7 install -r requirements.txt
```
